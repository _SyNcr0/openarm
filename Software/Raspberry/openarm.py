from flask import Flask, jsonify
from flask import request

import RPi.GPIO as gpio
import time
import serial

app = Flask(__name__)
gpio.setmode(gpio.BCM)
gpio.setwarnings(False)
ser = serial.Serial('/dev/ttyUSB0', 9600)

@app.route("/")	
def web_interface():
  html = open("index.html")
  response = html.read().replace('\n', '')
  html.close()
  return response
  
@app.route("/set_servo1")  
#Gripper
def set_servo1():  
  angle = request.args.get("angle")
  print "Ricevuto " + str(angle)
  send = "7"+angle
  ser.write(send.encode())
  print("Esecuzione: " + ser.readline());
  return jsonify('{ "Operation": "OK"}')  
  
@app.route("/set_servo2")  
#Polso
def set_servo2():  
  angle = request.args.get("angle")
  print "Ricevuto " + str(angle)
  send = "6"+angle
  ser.write(send.encode())
  print("Esecuzione: " + ser.readline());
  return jsonify('{ "Operation": "OK"}')  

@app.route("/set_servo3")  
#Braccio - parte superiore
def set_servo3():  
  angle = request.args.get("angle")
  print "Ricevuto " + str(angle)
  send = "5"+angle
  ser.write(send.encode())
  print("Esecuzione: " + ser.readline());
  return jsonify('{ "Operation": "OK"}')  

@app.route("/set_servo4")  
#Braccio - parte intermedia
def set_servo4():  
  angle = request.args.get("angle")
  print "Ricevuto " + str(angle)
  send = "4"+angle
  ser.write(send.encode())
  print("Esecuzione: " + ser.readline());
  return jsonify('{ "Operation": "OK"}')  
 
@app.route("/set_servo5")  
#Braccio - parte inferiore
def set_servo5():  
  angle = request.args.get("angle")
  print "Ricevuto " + str(angle)
  send = "3"+angle
  ser.write(send.encode())
  print("Esecuzione: " + ser.readline());
  return jsonify('{ "Operation": "OK"}')  

@app.route("/set_servo6")  
#Base
def set_servo6():  
  angle = request.args.get("angle")
  print "Ricevuto " + str(angle)
  send = "2"+angle
  ser.write(send.encode())
  print("Esecuzione: " + ser.readline());
  return jsonify('{ "Operation": "OK"}')  
  
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=False)
