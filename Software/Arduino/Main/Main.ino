#include <Servo.h> 

Servo base;
Servo braccioInf;
Servo braccioInt;
Servo braccioSup;
Servo polso;
Servo gripper;
String response;
int angle;
int motor;
int i;

void setup() 
{ 
	Serial.begin(9600);
	gripper.write(90);
	gripper.attach(7);
	delay(200);
	polso.attach(6);
	delay(200);
	braccioSup.write(30);
	braccioSup.attach(5);
	delay(200);
	braccioInt.write(10);
	braccioInt.attach(4);
	delay(200);
	braccioInf.write(110);
	braccioInf.attach(3);
	delay(200);
	base.attach(2);
	delay(200);
	angle = 0;
	motor = 0;
} 
 
void loop() 
{ 
	if(Serial.available())
	{ 
		response = Serial.readString();
		motor = response.charAt(0) - '0';
        angle = response.substring(1).toInt();
		switch (motor)
		{
			case 7:
				if (gripper.read() < angle)
				{
					for (i = gripper.read(); i < angle; i++)
					{
						gripper.write(i);
						delay(30);
					}
					break;
				}
				for (i = gripper.read(); i > angle; i--)
				{
					gripper.write(i);
					delay(30);
				}
				break;
				
			case 6:
				if (polso.read() < angle)
				{
					for (i = polso.read(); i < angle; i++)
					{
						polso.write(i);
						delay(30);
					}
					break;
				}
				for (i = polso.read(); i > angle; i--)
				{
					polso.write(i);
					delay(30);
				}
				break;
				
			case 5:
				if (braccioSup.read() < angle)
				{
					for (i = braccioSup.read(); i < angle; i++)
					{
						braccioSup.write(i);
						delay(30);
					}
					break;
				}
				for (i = braccioSup.read(); i > angle; i--)
				{
					braccioSup.write(i);
					delay(30);
				}
				break;
				
			case 4:
				if (braccioInt.read() < angle)
				{
					for (i = braccioInt.read(); i < angle; i++)
					{
						braccioInt.write(i);
						delay(30);
					}
					break;
				}
				for (i = braccioInt.read(); i > angle; i--)
				{
					braccioInt.write(i);
					delay(30);
				}
				break;
				
			case 3:
				if (braccioInf.read() < angle)
				{
					for (i = braccioInf.read(); i < angle; i++)
					{
						braccioInf.write(i);
						delay(30);
					}
					break;
				}
				for (i = braccioInf.read(); i > angle; i--)
				{
					braccioInf.write(i);
					delay(30);
				}
				break;
				
			case 2:
				if (base.read() < angle)
				{
					for (i = base.read(); i < angle; i++)
					{
						base.write(i);
						delay(30);
					}
					break;
				}
				for (i = base.read(); i > angle; i--)
				{
					base.write(i);
					delay(30);
				}
				break;
		}	
		Serial.println("OK");
	}                         
} 
