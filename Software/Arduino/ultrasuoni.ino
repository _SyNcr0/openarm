//DT Studio
//Compatible with the Arduino IDE 1.0
//Library version:1.1
#include <Servo.h>

Servo servo;
const int trigPin = 9;
const int echoPin = 10;
int duration;
int distance;
int servoValue;

void setup()
{
  servo.attach(12);
  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input
  Serial.begin(9600); // Starts the serial communication
}

void loop()
{
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance= duration*0.034/2;
  Serial.print("Distanza: ");
  Serial.print(distance);
  Serial.println();
  Serial.println();

  if ( distance >= 5 && distance <= 25)
  {
    servoValue = distance * 9 - 45;
    Serial.print("Servo: ");
    Serial.print(servoValue);
    Serial.println();
    servo.write(servoValue);
    delay(30);
  }
  delay(200);
  
}
